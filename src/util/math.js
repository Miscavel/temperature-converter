/**
 * Round a number to a specific decimal place
 * @param {*} num The number to be rounded
 * @param {*} dp Decimal place
 * 
 * e.g. roundToDP(150.123, 2) => 150.12
 */
export function roundToDP(num, dp) {
  const multiplier = Math.pow(10, dp);
  return Math.round(num * multiplier) / multiplier;
}