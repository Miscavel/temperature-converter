import { createUseStyles, useTheme } from 'react-jss';
import ToggleButton from '../shared/ToggleButton';

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    width: '80%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: '20px 0px 20px 0px',
  },
  label: {
    fontWeight: 'bold',
    marginRight: '20px',
  }
}, {
  name: 'DarkModeToggle'
});

function DarkModeToggle({ isDarkMode, onDarkModeToggle }) {
  const theme = useTheme();
  const { main, label } = useStyles({ theme });

  return (
    <div className={ main }>
      <div className={ label }>
        Dark Mode
      </div>
      <ToggleButton
        defaultActive={ isDarkMode }
        onToggle={ onDarkModeToggle }
      >
      </ToggleButton>
    </div>
  );
}

export default DarkModeToggle;