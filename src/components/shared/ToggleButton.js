import { useState } from 'react';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    border: '1px solid',
    minWidth: '60px',
    height: '30px',
    borderRadius: '20px',
    alignItems: 'center',
    padding: '0px 3px 0px 3px',
    backgroundColor: function({ active }) {
      return active ? '#0D6EFD' : 'white';
    },
    borderColor: function({ active }) {
      return active ? '#0D6EFD' : '#BFBFBF';
    },
    cursor: 'pointer',
    WebkitTapHighlightColor: 'transparent',
  },
  button: {
    width: '25px',
    height: '25px',
    borderRadius: '25px',
    backgroundColor: function({ active }) {
      return active ? 'white' : '#BFBFBF';
    },
    transform: function({ active }) {
      return active ? 'translateX(35px)' : 'translateX(0)';
    },
    transition: 'transform 0.3s',
  }
}, {
  name: 'ToggleButton'
});

function ToggleButton({ defaultActive = false, onToggle }) {
  const [ active, setActive ] = useState(defaultActive);
  const { main, button } = useStyles({ active });

  function toggleActive() {
    const newState = !active;
    setActive(newState);
    onToggle(newState);
  }

  return (
    <div 
      className={ main }
      onClick={ toggleActive }
    >
      <div 
        className={ button }
      >
      </div>
    </div>
  )
}

export default ToggleButton;