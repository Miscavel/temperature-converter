import { createUseStyles, useTheme } from 'react-jss';

const useStyles = createUseStyles({
  inputGroup: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    border: '1px solid',
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    borderRadius: '2px',
    marginBottom: '20px',
  },
  inputDegree: {
    border: 'none',
    borderRadius: '2px 2px 0px 0px',
    borderBottom: '1px solid',
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    outline: 'none',
    padding: '10px',
    fontSize: '20px',
    backgroundColor: function({ theme }) {
      return theme.bgColor;
    },
    color: function({ theme }) {
      return theme.primaryTextColor;
    },
  },
  selectUnit: {
    border: 'none',
    borderRadius: '0px 0px 2px 2px',
    outline: 'none',
    backgroundColor: function({ theme }) {
      return theme.panelColor;
    },
    padding: '10px',
    color: function({ theme }) {
      return theme.secondaryTextColor;
    },
  },
  optionUnit: {

  },
}, {
  name: 'TemperatureInput'
});

function TemperatureInput({ options, selectedUnit, onUnitChange, temperature, onTemperatureChange }) {
  const theme = useTheme();
  const { inputGroup, inputDegree, selectUnit, optionUnit } = useStyles({ theme });

  function onInputChange(event) {
    onTemperatureChange(event.currentTarget.value);
  }

  function onSelectChange(event) {
    onUnitChange(event.currentTarget.selectedIndex);
  }

  return (
    <div className={ inputGroup }>
      <input
        className={ inputDegree }
        type='number'
        value={ temperature }
        onChange={ onInputChange }
      >
      </input>
      <select 
        className={ selectUnit }
        value={ options[selectedUnit].value }
        onChange={ onSelectChange }
      >
        {
          options &&
          options.map(function(option, index) {
            const { value, label, symbol } = option;
            return (
              <option 
                className={ optionUnit }
                value={ value } 
                key={ index }
              >
                { label } ({ symbol })
              </option>
            )
          })
        }
      </select>
    </div>
  )
}

export default TemperatureInput;