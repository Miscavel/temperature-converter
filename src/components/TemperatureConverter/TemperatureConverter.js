import { useState } from 'react';
import TemperatureInput from './TemperatureInput';
import FormulaBox from './FormulaBox';
import { roundToDP } from '../../util/math';
import { createUseStyles, useTheme } from 'react-jss';

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    flexDirection: 'column',
    border: '1px solid',
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    borderRadius: '10px',
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '30px',
    boxSizing: 'border-box',
  },
}, {
  name: 'TemperatureConverter'
});

function TemperatureConverter({ options }) {
  const theme = useTheme();
  const { main } = useStyles({ theme });

  /**
   * Selected units in terms of options index (see getTemperatureUnitOptions for reference)
   * 
   * e.g.
   * 0 => options[0] => { value: 'celsius', label: 'Celsius', symbol: '°C' }
   * 2 => options[2] => { value: 'kelvin', label: 'Kelvin', symbol: 'K' }
   */
  const [ selectedUnits, setSelectedUnits ] = useState([0, 1]);

  /**
   * Temperatures, start at 0 degrees
   */
  const [ temperatures, setTemperatures ] = useState(['0', '32']);

  /**
   * Handles temperature unit change
   * 
   * @param {*} thisIndex Index referring to the unit that changed
   * @param {*} thatIndex Index referring to the unit that did not change
   * @param {*} newUnit New value of the unit that changed
   */
  function onUnitChange(thisIndex, thatIndex, newUnit) {
    const thisUnit = selectedUnits[thisIndex];
    const thatUnit = selectedUnits[thatIndex];

    /**
     * If the new value matches the unchanged unit, swap the units
     * 
     * e.g.
     * selectedUnits: [0 ('celsius'), 2 ('kelvin')];
     * 
     * Invoke: onUnitChange(0, 1, 2 ('kelvin'));
     * 
     * Since 2 ('kelvin') === selectedUnits[1],
     * selectedUnits(new): [2 ('kelvin'), 0 ('celsius')];
     */
    if (newUnit === thatUnit) {
      selectedUnits[thatIndex] = thisUnit;
    }
    selectedUnits[thisIndex] = newUnit;

    setSelectedUnits([...selectedUnits]);

    // Update temperatures to match the new units
    onTemperatureChange(0, 1, temperatures[0]);
  }

  function onTemperatureChange(thisIndex, thatIndex, newTemperature) {
    const thisUnit = selectedUnits[thisIndex];
    const thatUnit = selectedUnits[thatIndex];

    const thisOption = options[thisUnit];
    const thatOption = options[thatUnit];

    const temperatureInNumber = Number(newTemperature);
    
    if (
      newTemperature !== '' &&
      !isNaN(temperatureInNumber)
    ) {
      temperatures[thatIndex] = String(
        roundToDP(thisOption.convert(temperatureInNumber, thatOption.value), 3)
      );
    } else {
      temperatures[thatIndex] = '';
    }
    temperatures[thisIndex] = newTemperature;
    
    setTemperatures([...temperatures]);
  }

  function getFormula() {
    const thisUnit = selectedUnits[0];
    const thatUnit = selectedUnits[1];

    const thisOption = options[thisUnit];
    const thatOption = options[thatUnit];

    const formulaTemplate = thisOption.getFormula(thatOption.value);
    return `
      ${formulaTemplate.replace('{temperature}', `${temperatures[0]}<b>${thisOption.symbol}</b>`)} 
      = ${temperatures[1]}<b>${thatOption.symbol}</b>
    `;
  }

  return(
    <div className={ main }>
      <TemperatureInput
        options={ options }
        selectedUnit={ selectedUnits[0] }
        onUnitChange={ 
          function(newUnit) {
            onUnitChange(0, 1, newUnit);
          } 
        }
        temperature={ temperatures[0] }
        onTemperatureChange={
          function(newTemperature) {
            onTemperatureChange(0, 1, newTemperature);
          }
        }
      >
      </TemperatureInput>
      <TemperatureInput
        options={ options }
        selectedUnit={ selectedUnits[1] }
        onUnitChange={ 
          function(newUnit) {
            onUnitChange(1, 0, newUnit);
          } 
        }
        temperature={ temperatures[1] }
        onTemperatureChange={
          function(newTemperature) {
            onTemperatureChange(1, 0, newTemperature);
          }
        }
      >
      </TemperatureInput>
      <FormulaBox formula={ getFormula() }>
      </FormulaBox>
    </div>
  );
}

export default TemperatureConverter;