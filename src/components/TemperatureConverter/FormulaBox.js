import { createUseStyles, useTheme } from 'react-jss';

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    border: '1px solid',
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    borderRadius: '2px',
    color: function({ theme }) {
      return theme.secondaryTextColor;
    },
  },
  title: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10px',
    borderBottom: '1px solid',
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    backgroundColor: function({ theme }) {
      return theme.panelColor;
    },
  },
  content: {
    padding: '10px',
  }
}, {
  name: 'FormulaBox'
});

function FormulaBox({ formula }) {
  const theme = useTheme();
  const { main, title, content } = useStyles({ theme });

  return (
    <div
      className={ main }
    >
      <div className={ title }>
        Formula
      </div>
      <div 
        className={ content }
        dangerouslySetInnerHTML={{ __html: formula }}
      >
      </div>
    </div>
  );
}

export default FormulaBox;