import { useState } from 'react';
import './App.css';
import TemperatureConverter from './components/TemperatureConverter/TemperatureConverter';
import DarkModeToggle from './components/DarkModeToggle/DarkModeToggle';
import { getTemperatureUnitOptions } from './config/temperature';
import { createUseStyles, ThemeProvider } from 'react-jss';

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: function({ theme }) {
      return theme.bgColor;
    },
    color: function({ theme }) {
      return theme.primaryTextColor;
    },
    overflow: 'auto',
  }
}, {
  name: 'App'
});

const lightTheme = {
  bgColor: 'white',
  primaryTextColor: 'black',
  secondaryTextColor: 'black',
  borderColor: '#DADCE0',
  panelColor: '#F8F9FA',
};

const darkTheme = {
  bgColor: '#202124',
  primaryTextColor: 'white',
  secondaryTextColor: '#BDC1C6',
  borderColor: '#3C4043',
  panelColor: '#303134',
};

function App() {
  const [ isDarkMode, setDarkMode ] = useState(false);
  const [ options ] = useState(getTemperatureUnitOptions());

  const theme = isDarkMode ? darkTheme : lightTheme;
  const { main } = useStyles({ theme });

  function onDarkModeToggle(active) {
    setDarkMode(active);
  }

  return (
    <ThemeProvider theme={ theme }>
      <div className={ main }>
        <DarkModeToggle
          isDarkMode={ isDarkMode }
          onDarkModeToggle={ onDarkModeToggle }
        >
        </DarkModeToggle>
        <TemperatureConverter
          options={ options }
        >
        </TemperatureConverter>
      </div>
    </ThemeProvider>
  );
}

export default App;
