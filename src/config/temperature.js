export function getTemperatureUnitOptions() {
  const unitOptions = [
    {
      value: 'celsius',
      label: 'Celsius',
      symbol: '°C',
      convert: function(temperature, unit) {
        switch(unit) {
          case 'fahrenheit': {
            return (temperature * 1.8) + 32;
          }

          case 'kelvin': {
            return temperature + 273.15;
          }

          case 'rankine': {
            return (temperature + 273.15) * 1.8;
          }

          case 'reaumur': {
            return temperature * 0.8;
          }

          default: {
            return temperature;
          }
        }
      },
      getFormula: function(unit) {
        switch(unit) {
          case 'fahrenheit': {
            return `({temperature} * 1.8) + 32`;
          }

          case 'kelvin': {
            return `{temperature} + 273.15`;
          }

          case 'rankine': {
            return `({temperature} + 273.15) * 1.8`;
          }

          case 'reaumur': {
            return `{temperature} * 0.8`;
          }

          default: {
            return `{temperature}`;
          }
        }
      }
    },
    {
      value: 'fahrenheit',
      label: 'Fahrenheit',
      symbol: '°F',
      convert: function(temperature, unit) {
        switch(unit) {
          case 'celsius': {
            return (temperature - 32) / 1.8;
          }

          case 'kelvin': {
            return (temperature + 459.67) / 1.8;
          }

          case 'rankine': {
            return temperature + 459.67;
          }

          case 'reaumur': {
            return (temperature - 32) * 4 / 9;
          }

          default: {
            return temperature;
          }
        }
      },
      getFormula: function(unit) {
        switch(unit) {
          case 'celsius': {
            return `({temperature} - 32) / 1.8`;
          }
        
          case 'kelvin': {
            return `({temperature} + 459.67) / 1.8`;
          }
        
          case 'rankine': {
            return `{temperature} + 459.67`;
          }
        
          case 'reaumur': {
            return `({temperature} - 32) * 4 / 9`;
          }
        
          default: {
            return `{temperature}`;
          }
        }
      }
    },
    {
      value: 'kelvin',
      label: 'Kelvin',
      symbol: 'K',
      convert: function(temperature, unit) {
        switch(unit) {
          case 'celsius': {
            return temperature - 273.15;
          }

          case 'fahrenheit': {
            return (temperature * 1.8) - 459.67;
          }

          case 'rankine': {
            return temperature * 1.8;
          }

          case 'reaumur': {
            return (temperature - 273.15) * 0.8;
          }

          default: {
            return temperature;
          }
        }
      },
      getFormula: function(unit) {
        switch(unit) {
          case 'celsius': {
            return `{temperature} - 273.15`;
          }
        
          case 'fahrenheit': {
            return `({temperature} * 1.8) - 459.67`;
          }
        
          case 'rankine': {
            return `{temperature} * 1.8`;
          }
        
          case 'reaumur': {
            return `({temperature} - 273.15) * 0.8`;
          }
        
          default: {
            return `{temperature}`;
          }
        }
      }
    },
    {
      value: 'rankine',
      label: 'Rankine',
      symbol: '°Ra',
      convert: function(temperature, unit) {
        switch(unit) {
          case 'celsius': {
            return (temperature - 491.67) / 1.8;
          }

          case 'fahrenheit': {
            return temperature - 459.67;
          }

          case 'kelvin': {
            return temperature / 1.8;
          }

          case 'reaumur': {
            return (temperature - 491.67) * 4 / 9;
          }

          default: {
            return temperature;
          }
        }
      },
      getFormula: function(unit) {
        switch(unit) {
          case 'celsius': {
            return `({temperature} - 491.67) / 1.8`;
          }
        
          case 'fahrenheit': {
            return `{temperature} - 459.67`;
          }
        
          case 'kelvin': {
            return `{temperature} / 1.8`;
          }
        
          case 'reaumur': {
            return `({temperature} - 491.67) * 4 / 9`;
          }
        
          default: {
            return `{temperature}`;
          }
        }
      }
    },
    {
      value: 'reaumur',
      label: 'Réaumur',
      symbol: '°Re',
      convert: function(temperature, unit) {
        switch(unit) {
          case 'celsius': {
            return temperature / 0.8;
          }

          case 'fahrenheit': {
            return (temperature * 9 / 4) + 32;
          }

          case 'kelvin': {
            return (temperature * 1.25) + 273.15;
          }

          case 'rankine': {
            return (temperature * 9 / 4) + 491.67;
          }

          default: {
            return temperature;
          }
        }
      },
      getFormula: function(unit) {
        switch(unit) {
          case 'celsius': {
            return `{temperature} / 0.8`;
          }
        
          case 'fahrenheit': {
            return `({temperature} * 9 / 4) + 32`;
          }
        
          case 'kelvin': {
            return `({temperature} * 1.25) + 273.15`;
          }
        
          case 'rankine': {
            return `({temperature} * 9 / 4) + 491.67`;
          }
        
          default: {
            return `{temperature}`;
          }
        }
      }
    },
  ];

  return unitOptions;
}