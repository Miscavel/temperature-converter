An application to convert temperatures between Celsius, Fahrenheit, Kelvin, Rankine, and Reaumur.

This project is live at: https://miscavel.gitlab.io/temperature-converter/

Table of Contents:

[[_TOC_]]

# Project Setup

## Node.js and npm

This project is built using [ReactJS](https://reactjs.org/versions/) version 18.2, and thus requires Node.js to be installed to run the development server and install its dependencies. 

Check [the official documentation](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) for more detailed information on how to install it on your operating system.

This project uses `Node.js` version 14.1.0, although other unspecified versions would work as long as they support the [installed packages](https://gitlab.com/Miscavel/temperature-converter/-/blob/main/package.json).

## Installing Dependencies

Once `npm` has been installed successfully, run `npm install` inside the project's directory to install the required dependencies.

## Running Development Server

Run `npm start` inside the project's directory to start the development server.

The project will be hosted at http://localhost:3000 by default.

## Building App for Production

Run `npm run build` inside the project's directory to create the production bundles.

The bundles can then be found inside the `/build` folder under the project's root directory.

## Deploying Production App

To deploy the built production app, simply host the `/build` folder on your server of choice.

An example of how to host it on gitlab pages can be found inside [.gitlab-ci.yml](https://gitlab.com/Miscavel/miscavel.gitlab.io/-/blob/main/.gitlab-ci.yml)

# Libraries

## [ReactJS](https://reactjs.org/)

ReactJS is used for its stateful pattern through the use of hooks such as [useState](https://reactjs.org/docs/hooks-reference.html#usestate) and [useRef](https://reactjs.org/docs/hooks-reference.html#useref).

This allows us to store values such as `darkMode`, `temperatureUnits`, and `temperatureValues`, and re-render the page whenever any of those values change.

## [CSS-in-JS](https://cssinjs.org/)

CSS-in-JS is used primarily to scope and isolate the styles of each component.

It also comes with [Theming](https://github.com/cssinjs/theming) support, which helps in setting different styles for `lightMode` and `darkMode` through the use of `ThemeProvider` wrapper.

# UI Design

## Temperature Converter

The app design is heavily adopted from google's temperature converter intuitive design.

![UIDesign_Comparison](images/uidesign_comparison.png)

1. When any of the temperature value is changed, the other temperature value is going to change accordingly following their conversion formula.

![UIDesign_Temperature_Value_Change](images/uidesign_temperature_value_change.gif)

2. When any of the temperature unit is changed, the bottom temperature value is going to change according to the new conversion formula while the top temperature value remains unchanged. 

![UIDesign_Temperature_Unit_Change](images/uidesign_temperature_unit_change.gif)

These 2 systems combined allows for ease of conversion between 2 temperature units at any given time.

## Dark Mode

users can turn on and off the switch at the top right corner to toggle between dark and light mode respectively. 

This design is chosen for its ease of access in both PC and mobile layout.

![UIDesign_Dark_Mode](images/uidesign_dark_mode.gif)

# System Design

## Temperature Converter

### Unit Options Config

The core of the temperature conversion logic can be found in the [temperature units configuration file](https://gitlab.com/Miscavel/temperature-converter/-/blob/main/src/config/temperature.js).

This config lists down all the available units for conversion, including their conversion formulas wrapped within the `convert` function.

```
export function getTemperatureUnitOptions() {
  const unitOptions = [
    {
      value: 'celsius',
      label: 'Celsius',
      symbol: '°C',
      convert: function(temperature, unit) {
        switch(unit) {
          case 'fahrenheit': {
            return (temperature * 1.8) + 32;
          }

          case 'kelvin': {
            return temperature + 273.15;
          }

          case 'rankine': {
            return (temperature + 273.15) * 1.8;
          }

          case 'reaumur': {
            return temperature * 0.8;
          }

          default: {
            return temperature;
          }
        }
      },
      getFormula: function(unit) {
        switch(unit) {
          case 'fahrenheit': {
            return `({temperature} * 1.8) + 32`;
          }

          case 'kelvin': {
            return `{temperature} + 273.15`;
          }

          case 'rankine': {
            return `({temperature} + 273.15) * 1.8`;
          }

          case 'reaumur': {
            return `{temperature} * 0.8`;
          }

          default: {
            return `{temperature}`;
          }
        }
      }
    },
    ...
  ];

  return unitOptions;
}
```

Conversion from `celsius` to `fahrenheit` can then be done by invoking:
```
unitOptions.find(function(unit) {
    return unit.value === 'celsius';    
}).convert(temperature, 'fahrenheit');
```

This object-oriented approach is chosen for ease of access from within the component, so that once we select a temperature unit we can simply call `selectedUnit.convert()` to get the converted value.

### OnTemperatureChange

This whole conversion system is managed by the [onTemperatureChange](https://gitlab.com/Miscavel/temperature-converter/-/blob/main/src/components/TemperatureConverter/TemperatureConverter.js#L77) function found in the `TemperatureConverter` component.

```
function onTemperatureChange(thisIndex, thatIndex, newTemperature) {
    const thisUnit = selectedUnits[thisIndex]; // Temperature that the user changes manually
    const thatUnit = selectedUnits[thatIndex]; // Temperature that changes automatically through conversion formula

    const thisOption = options[thisUnit];
    const thatOption = options[thatUnit];

    const temperatureInNumber = Number(newTemperature);
    
    if (
      newTemperature !== '' &&
      !isNaN(temperatureInNumber)
    ) {
      temperatures[thatIndex] = String(
        roundToDP(thisOption.convert(temperatureInNumber, thatOption.value), 3)
      );
    } else {
      temperatures[thatIndex] = '';
    }
    temperatures[thisIndex] = newTemperature;
    
    setTemperatures([...temperatures]);
  }
```
`thisIndex` refers to the index of the temperature component which value has changed, whereas `thatIndex` refers to the temperature component which value changes automatically as a result of the auto-conversion. 

Since there are only 2 temperature components (top and bottom), the values for these indexes can only be either `0` (top) or `1` (bottom).

As an example, when:
1. The top temperature value is changed to 12, `onTemperatureChange(0, 1, 12)` is invoked
2. The bottom temperature value is changed to 50, `onTemperatureChange(1, 0, 50)` is invoked

### OnUnitChange

Temperature unit change is managed by the [onUnitChange](https://gitlab.com/Miscavel/temperature-converter/-/blob/main/src/components/TemperatureConverter/TemperatureConverter.js#L51) function found in the same `TemperatureConverter` component.

```
/**
   * Handles temperature unit change
   * 
   * @param {*} thisIndex Index referring to the unit that changed
   * @param {*} thatIndex Index referring to the unit that did not change
   * @param {*} newUnit New value of the unit that changed
   */
  function onUnitChange(thisIndex, thatIndex, newUnit) {
    const thisUnit = selectedUnits[thisIndex];
    const thatUnit = selectedUnits[thatIndex];

    /**
     * If the new value matches the unchanged unit, swap the units
     * 
     * e.g.
     * selectedUnits: [0 ('celsius'), 2 ('kelvin')];
     * 
     * Invoke: onUnitChange(0, 1, 2 ('kelvin'));
     * 
     * Since 2 ('kelvin') === selectedUnits[1],
     * selectedUnits(new): [2 ('kelvin'), 0 ('celsius')];
     */
    if (newUnit === thatUnit) {
      selectedUnits[thatIndex] = thisUnit;
    }
    selectedUnits[thisIndex] = newUnit;

    setSelectedUnits([...selectedUnits]);

    // Update temperatures to match the new units
    onTemperatureChange(0, 1, temperatures[0]);
  }
```
It follows the same `thisIndex` and `thatIndex` rule as `onTemperatureChange`.

Whenever **any** of the component's temperature unit is changed, **the bottom component's temperature value is always the one that is converted to follow the new formula**, hence why `onTemperatureChange(0, 1, temperatures[0]);` is invoked.

### General Flow

![UIDesign_Temperature_Value_Change](images/temperature_converter_flow.png)

## Dark Mode

The dark mode system is implemented by passing different themed styles based on whether `isDarkMode` is true or false.
```
const lightTheme = {
  bgColor: 'white',
  primaryTextColor: 'black',
  secondaryTextColor: 'black',
  borderColor: '#DADCE0',
  panelColor: '#F8F9FA',
};

const darkTheme = {
  bgColor: '#202124',
  primaryTextColor: 'white',
  secondaryTextColor: '#BDC1C6',
  borderColor: '#3C4043',
  panelColor: '#303134',
};

function App() {
  ...
  const [ isDarkMode, setDarkMode ] = useState(false);
  const theme = isDarkMode ? darkTheme : lightTheme;
  const { main } = useStyles({ theme });

  function onDarkModeToggle(active) {
    setDarkMode(active);
  }

  return (
    <ThemeProvider theme={ theme }>
      <div className={ main }>
        <DarkModeToggle
          isDarkMode={ isDarkMode }
          onDarkModeToggle={ onDarkModeToggle }
        >
        </DarkModeToggle>
        ...
      </div>
    </ThemeProvider>
  );
}
```

With the help of `ThemeProvider` wrapper the themed styles can be retrieved by calling `useTheme`, which can then be used to dynamically change the style of any component within the wrapper.

Below is an example of how the theme is integrated into the [FormulaBox](https://gitlab.com/Miscavel/temperature-converter/-/blob/main/src/components/TemperatureConverter/FormulaBox.js) component.

```
const useStyles = createUseStyles({
  main: {
    ...,
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    color: function({ theme }) {
      return theme.secondaryTextColor;
    },
  },
  title: {
    ...,
    borderColor: function({ theme }) {
      return theme.borderColor;
    },
    backgroundColor: function({ theme }) {
      return theme.panelColor;
    },
  },
}, {
  name: 'FormulaBox'
});

function FormulaBox({ formula }) {
  const theme = useTheme();
  const { main, title, content } = useStyles({ theme });

  return (
    <div
      className={ main }
    >
      <div className={ title }>
        Formula
      </div>
      <div 
        className={ content }
        dangerouslySetInnerHTML={{ __html: formula }}
      >
      </div>
    </div>
  );
}
```
